#!/bin/bash

set -euo pipefail

# ALGO can be one of: rsa2048, ec384, ec256

export SSLCERT=ssl/$ALGO.pem
export SSLKEY=ssl/$ALGO.key

./build/server &
pid=$!
./build/client
kill -INT "$pid"

go tool pprof -raw pprof/client/cpu.pprof | stackcollapse-go.pl | flamegraph.pl --hash > pprof/client/$ALGO.svg
go tool pprof -raw pprof/server/cpu.pprof | stackcollapse-go.pl | flamegraph.pl --hash > pprof/server/$ALGO.svg
