package main

import (
	"crypto/tls"
	"crypto/x509"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/pkg/profile"
)

func main() {
	defer profile.Start(profile.ProfilePath("pprof/client")).Stop()

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT)
	done := make(chan bool, 1)

	go func() {
		<-sigs
		close(done)
	}()

	caCert, err := ioutil.ReadFile(os.Getenv("SSLCERT"))
	if err != nil {
		log.Fatal(err)
	}
	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)

	for i := 0; i < 10000; i++ {
		doneb := false
		select {
		case <-done:
			doneb = true
		default:
		}
		if doneb {
			break
		}

		client := &http.Client{
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{
					RootCAs: caCertPool,
				},
			},
		}

		resp, err := client.Get("https://localhost:9443")
		if err != nil {
			log.Printf("failed to make http request: %v", err)
			continue
		}
		resp.Body.Close()
	}
}
