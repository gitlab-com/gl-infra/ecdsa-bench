package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/pkg/profile"
	"github.com/shirou/gopsutil/v3/cpu"
)

func main() {
	defer profile.Start(profile.ProfilePath("pprof/server")).Stop()

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT)
	done := make(chan bool, 1)

	go func() {
		<-sigs
		close(done)
	}()

	go func() {
		<-done

		stats, err := cpu.Times(false)
		if err != nil {
			log.Fatalf("cpu.Times: %v", err)
		}
		fmt.Printf("cpu: %v (user %v system %v)\n", stats[0].User+stats[0].System, stats[0].User, stats[0].System)
	}()

	http.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Content-Type", "text/plain")
		w.Write([]byte("This is an example server.\n"))
	})
	err := http.ListenAndServeTLS("127.0.0.1:9443", os.Getenv("SSLCERT"), os.Getenv("SSLKEY"), nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
