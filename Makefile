.PHONY: build
build:
	mkdir -p build
	go build -o build ./...

.PHONY: certs
certs:
	mkdir -p ssl

	printf "subjectAltName=DNS:localhost,IP:127.0.0.1" > ssl/extfile.out

	openssl req -new -subj "/C=US/ST=California/CN=localhost" -newkey rsa:2048 -nodes -keyout ssl/rsa2048.key -out ssl/rsa2048.csr
	openssl x509 -req -days 365 -in ssl/rsa2048.csr -signkey ssl/rsa2048.key -out ssl/rsa2048.pem -extfile ssl/extfile.out

	openssl ecparam -name secp384r1 > ssl/ecparam.out
	openssl req -new -subj "/C=US/ST=California/CN=localhost" -newkey ec:ssl/ecparam.out -nodes -keyout ssl/ec384.key -out ssl/ec384.csr
	openssl x509 -req -days 365 -in ssl/ec384.csr -signkey ssl/ec384.key -out ssl/ec384.pem -extfile ssl/extfile.out

	openssl ecparam -name secp256r1 > ssl/ecparam.out
	openssl req -new -subj "/C=US/ST=California/CN=localhost" -newkey ec:ssl/ecparam.out -nodes -keyout ssl/ec256.key -out ssl/ec256.csr
	openssl x509 -req -days 365 -in ssl/ec256.csr -signkey ssl/ec256.key -out ssl/ec256.pem -extfile ssl/extfile.out

.PHONY: bench
bench: build
	./bench.sh
