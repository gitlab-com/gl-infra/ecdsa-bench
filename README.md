# ecdsa-bench

Benchmarking RSA vs ECDSA in the context of a golang HTTPS server.

## Usage

```
$ make build
$ make certs
$ time ALGO=rsa2048 make bench
$ time ALGO=ec384 make bench
$ time ALGO=ec256 make bench

$ open pprof/server/*.svg
```
